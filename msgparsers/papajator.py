import os
from random import choice

from base import BaseMessageParser
from libs.faceswap.swap_api import SwapFaces


def faces():
    files = os.listdir('libs/faceswap/data')
    return [fn.split('.')[0] for fn in files]


class Papajator(BaseMessageParser):
    def __init__(self):
        self.swapper = SwapFaces()
        self.slack_cli = None
    
    def handle_message(self, event, bot_id):
        if event.get('user','') == bot_id:
            return

        if 'files' not in event:
            return

        print(event)
        message = event.get('text', '')
        if not message.startswith('kek'):
            return

        faces_list = faces()
        command = message.split(' ')
        try:
            face = command[1]
            if face not in faces_list:
                raise IndexError  # xD
        except IndexError:
            face = choice(faces_list)

        url = event['files'][0]['url_private_download']
        mime = event['files'][0]['mimetype']
        channel = event['channel']

        swapped = self.swapper.swap('libs/faceswap/data/%s.jpg' % face, url, mime)
        if swapped:
            resp = self.slack_cli.api_call(
                "files.upload",
                channels=[channel],
                file=open(swapped, 'rb'),
                title="hehehe"
            )
            os.unlink(swapped)
