import datetime
from random import choice
from base import BaseCommand

Q1 = [
    [
        'Ale numer... ',
        'Powierdzone info!',
        'Słow nie ma co oni wyprawiają',
        'Puść dalej!',
        'Szok, nie moge uwierzyć',
        'A to już słyszałeś?',
    ],
    [
        'Wiarygodne źrodlo',
        'Moja koleżanka',
        'Moj kuzyn',
        'Syn sąsiada',
        'Brat naszego proboszcza',
        'Kumpel mojego szefa',
        'feminazistki',
    ],
    [
        'z instytutu chorob zakaznych',
        'pracuje w strukturach rządowych i',
        'oficer ABW',
        'lekarz-epidemiolog',
        'dziennikarz w TVP',
        'szkolny kolega Agaty Dudy, no wiesz prezydentowej',
    ],
    [
        'dał(a) cynk że prezydent',
        'właśnie pisze mi w mailu że rząd',
        'wie na 100% że sejm',
        'przesłał(a) mi SMS-a że ministerswo zdrowia',
        'właśnie dostał(a) kominikat że sztab WP',
        'przeypadkiem dowiedział(a) się że premier',
    ],
    [
        'jutro w nocy',
        'w ciagu 24 godizn',
        'za chwilę',
        'w najbliższych godzinach',
        'w najbliższą sobotę',
        'na dniach',
    ],
    [
        'wyśle wszystkich 60+ do ośrodkow izolacyjnych.',
        'zamknie wjazd do Katowic.',
        'nakaże zamknięcie sklepow.',
        'nakaże objęcie Jerzego kwarantanną.',
        'wprowadzi wojsko na ulice.',
        'ma zakazać używania gotowki',
        'zablokuje konta bankowe od osob na litery A do L...',
    ],
]


class KoronaNews(BaseCommand):
    command = 'koronanews'
    quotes = [Q1]

    def handle_command(self, command, channel, evt):
        quotes = choice(self.quotes)
        return ':koronawirus: {}'.format(' '.join([choice(lines) for lines in quotes]))
