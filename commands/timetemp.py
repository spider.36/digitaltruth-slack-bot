import re
from datetime import timedelta
from math import log10, exp

from base import BaseCommand

TIME_RE = re.compile('\d+:\d+ \d+')


def parse_minutes(min_str):
    min_arr = min_str.split(':')
    minutes = int(min_arr[0])
    seconds = int(min_arr[1])
    return timedelta(minutes=minutes, seconds=seconds).total_seconds()


def convert(new_temp, base_time, base_temp=20):
    new_time = base_time * exp(-0.081 * (new_temp - base_temp))

    ret = str(timedelta(seconds=new_time)).split('.')[0]
    if ret[0:2] == '0:':
        return ret[2:]
    else:
        return ret


class TimeTempConverter(BaseCommand):
    command = 'convert'

    def handle_command(self, command, channel, evt):
        args = self.get_args(command)
        if not TIME_RE.match(args):
            return 'Zapytaj mnie o "min:sec nowa_temperatura", albo proszę spierdalać xD'
        argz = args.split(' ')
        try:
            seconds = parse_minutes(argz[0])
        except ValueError:
            return 'Coś popierdoliłeś z tym czasem'

        try:
            temp = int(argz[1])
        except (ValueError, IndexError):
            return 'Coś pojebałeś z temperaturą'

        if temp < 17:
            return 'Zanuż do ciepłej kąpieli albo wsadź w dupsko i nie wydurniaj się xD Jebie mnie to gdzie trzymasz butelki.'
        if temp > 40:
            return 'Niebezpieczeństwo! Oparzysz się debilu! To nie herbatka tylko kurwa film!'
        conv = convert(temp, seconds)

        return 'Nowy czas to %s' % conv
