import datetime
from random import randint

import requests
from bs4 import BeautifulSoup

from base import BaseCommand


class Hate(BaseCommand):
    command = 'hate'

    pages = 3

    def get_page(self):
        resp = requests.get('https://www.adl.org/hatesymbolsdatabase?page={}'.format(randint(0, self.pages)))
        return BeautifulSoup(resp.content, 'lxml')

    def get_entry(self):
        doc = self.get_page()
        col = doc.find('div', {'class': 'views-content'}).find_all('article')
        cnt = len(col)
        item = col[randint(0, cnt - 1)]
        url = item.select('img[src*=http]')[0]['src']
        descr = item.find('div', {'class': 'field'}).text
        return url, descr

    def handle_command(self, command, channel, evt):
        user = evt['user']

        url, description = self.get_entry()
        return '<@{}> Twój losowy symbol nienawiści to {} - {}'.format(user, url, description)
