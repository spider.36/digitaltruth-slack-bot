from commands.answers import SimpleAnswer
from commands.digitaltruth import DigitaltruthCommand
from commands.dilution import DilutionCalculator
from commands.hate import Hate
from commands.krul import Krul
from commands.misc import BotVersion
from commands.ruletka import Ruletka
from commands.swaplist import SwapList
from commands.timetemp import TimeTempConverter
from commands.korona import KoronaNews

from base import main_loop
from msgparsers.papajator import Papajator
from msgparsers.spodek import SpodekXD

COMMANDS = [
    DigitaltruthCommand(),
    BotVersion(),
    SimpleAnswer(),
    SwapList(),
    DilutionCalculator(),
    TimeTempConverter(),
    Ruletka(),
    Hate(),
    Krul(),
    KoronaNews(),
]
PARSERS = [Papajator(), SpodekXD()]

if __name__ == "__main__":
    main_loop(COMMANDS, PARSERS)
